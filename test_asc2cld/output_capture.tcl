# Tcl script for OpenMSX to capture the textual output of the MSX to a file
#
# Written by Pedro Gimeno Fortea, January 2020.
# Donated to the public domain.
# Special thanks to IRC users Vampier and BiFi for their help getting this to
# work.
#
# Usage: openmsx -script output_capture.tcl <rest_of_options>
# The output is written to a file called msx.out in the current directory.

debug set_bp 0x18 {[pc_in_slot 0 0]} {output_txt::out_char}

namespace eval output_txt {
  variable fout [open "msx.out" w]

  proc out_char {} {
    variable fout
    puts -nonewline $fout [format %c [reg A]]
  }
}
