#!/bin/sh
set -e
rm -f .coverage
./run-single-test.sh good1
./run-single-test.sh good2
./run-single-test.sh ovflow01
./run-single-test.sh ovflow02
./run-single-test.sh ovflow03
./run-single-test.sh ovflow04
./run-single-test.sh ovflow05
./run-single-test.sh ovflow06
./run-single-test.sh ovflow07
./run-single-test.sh ovflow08
./run-single-test.sh undline
./run-single-test.sh direcstm
python3-coverage html
echo "All tests passed"
