This zipfile contains:

  - Binaries resulting from the build
    vdptest.cas
    vdptest.bas
    vdptest.bin
    vdptest1.dsk (1-side 360K floppy image)
    vdptest2.dsk (2-side 720K floppy image)

  - Source code
    vdptest.asm
    sysdefs.equ.asm
    main.code.asm
    main.bss.asm
    im2setup.equ.asm
    im2setup.code.asm
    frametime.code.asm
    frametime.bss.asm
    syncvint.code.asm
    test-ack-timing.code.asm
    test-ack-timing.bss.asm
    loadertpl.py (generates BASIC loader for .cas or .dsk as appropriate)

  - Documentation
    USAGE.txt (this file)
    LICENSE.md (Expat License, the license that applies to the code)

  - Build makefile
    Makefile

  - Auxiliary Python programs used to build the output
    asc2cas.py
    asc2cld.py
    raw2cas.py
    raw2bin.py

  - Blank disk images
    blank1.dsk.gz (1-side 360K)
    blank2.dsk.gz (2-side 720K)

Requisites to build:

    To build using Makefile, you need GNU make.

    To assemble, you need Pasmo.

    To create the .cas image or the .bin disk file, you need Python 3 and cat.

    To automatically build the disk images, you need mtools and gzip.

    To create the .zip file, you additionally need rm and zip.

    To clean up the generated files, you need rm.

    Building generates some auxiliary files:
      vdptest.raw
      vdptestc.sym
      vdptestd.sym
      loader.cld
      loader.cas

    These files are not necessary once the build is complete, but re-running
    'make' may cause them to be generated again.

Usage:

    To build everything, type: make

    Other targets: make zip, make bas, make bin, make cas, make dsk, make clean

    To clean the directory and leave only the build requisites and sources,
    type: make clean

    To run it in a real MSX, you can use this with a cassette recorder or a
    floppy drive.

    Currently it only runs on a 16K+ machine. If run from floppy, disable the
    second drive by keeping CTRL pressed during boot.

Usage with a cassette recorder:

    vdptest.cas is usable by an emulator, but in order to write it to tape,
    you need to convert it first to .wav format, then play that .wav while
    you record in the cassette recorder.

    Tools that can convert the .cas file to .wav are not included. You can
    use cas2wav from Vincent Van Dam's castools, or imgtool which comes with
    MAME/MESS.

    castools can be found here: https://github.com/joyrex2001/castools

    With cas2wav:
      cas2wav vdptest.cas vdptest.wav

    With imgtool (untested):
      imgtool get fmsx_cas vdptest.cas vdptest.wav

    Either use vdptest.cas and cas2wav to generate a tape, or vdptest.bas

Usage with a floppy drive:

    Write vdptest.bas and vdptest.bin to a floppy and use RUN"vdptest.bas"

    The pre-built disk images vdptest1.dsk and vdptest2.dsk are provided for
    convenience.
