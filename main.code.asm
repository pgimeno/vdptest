; Main starting point
Main		proc

		ld	a,i
		ld	(SaveI),a
		ld	(SaveSP),sp

		call	SetupIM2

		; This is a prerequisite for all others
		call	TestCycPerFrame

		; First battery of tests: Interrupt Ack timing
		call	TestAckTiming

		; Second battery of tests: VRAM access timing
		call	TestVRAMTiming

		xor	a
		;jp	Finish
		; Fall through instead

		endp


; Clean up and exit (A=exit code)
Finish		proc

		di
		ld	sp,(SaveSP)

		ld	(DAC+2),a	; Save exit code

		in	a,(99h)		; Ensure we're in phase with the VDP port

		ex	(sp),hl		; Delay (1/2)

		ld	a,0
		out	(99h),a		; Select status register 0

		ex	(sp),hl		; Delay (2/2)

		ld	a,128+15
		out	(99h),a		; In V9938, write to register 15
					; In MSX1 type VDPs, this is written to register 7
					; so we need to restore it

		ex	(sp),hl		; Delay (1/2)

		ld	a,(RG0SAV+7)
		out	(99h),a		; Retrieve saved value

		ex	(sp),hl		; Delay (2/2)

		ld	a,128+7
		out	(99h),a		; Restore register 7

		ei
		im	1

		xor	a
		ld	(DAC+3),a
		ld	l,a
		ld	h,a
		ld	(DAC),hl
		ld	(DAC+4),hl
		ld	(DAC+6),hl
		ld	a,2
		ld	(VALTYP),a
		ld	a,(SaveI)
		ld	i,a

;		call	KILBUF
;		ret
		jp	KILBUF		; Do a tail call instead

		endp
