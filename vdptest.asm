		IF	!DEFINED origin
		.ERROR	"Run pasmo with: --equ origin=<addr>"
		ENDIF


; Pasmo macros for reserving space in bss

; This segment starts after the binary of this program
lodata		macro
HILO		defl	0
		org	LOptr
		endm

; This segment starts at VOICAQ; should not be greater than 132 bytes.
hidata		macro
HILO		defl	1
		org	HIptr
		endm

updateHILO	macro

		if HILO = 0
LOptr		defl	$
		else
HIptr		defl	$

		if HIptr > 0F9F9h
		.error	"Too many variables"
		endif

		endif

		endm


word		macro	arg

		if nul arg
		org	$ + 2
		else
		org	$ + 2*arg
		endif

		updateHILO

		endm

byte		macro	arg

		if nul arg
		org	$ + 1
		else
		org	$ + arg
		endif

		updateHILO

		endm

; ********************************* definitions

		include	'sysdefs.equ.asm'
		include	'im2setup.equ.asm'

; ********************************* entry point

		org	origin

Start		proc

		jp	Main

		endp

; *************** start of data area

; nothing to see here, move on

; *************** end of data area, start of code area

		include	'main.code.asm'
		include	'im2setup.code.asm'
		include 'frametime.code.asm'
		include	'syncvint.code.asm'
		include	'test-ack-timing.code.asm'
		include	'test-vram-timing.code.asm'

; *************** end of code area, start of bss area

LOptr		defl	$
HIptr		defl	VOICAQ

		include	'frametime.bss.asm'
		include	'test-ack-timing.bss.asm'
		include	'main.bss.asm'
		include	'test-vram-timing.bss.asm'

		lodata
LastByte	equ	LOptr

; *************** end of everything

		end
