; Data for main

		hidata

ErrParams	byte	4

; Save the I register. There's no technical reason, it's just that I like leaving things
; the same way I find them :)
SaveI		byte
; Allows a "panic exit"
SaveSP		word
