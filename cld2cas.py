#!/usr/bin/env python3

# Written by Pedro Gimeno
# args: infile outfile savename

import sys, struct

class FileFormatError(Exception):
  pass

def main():
  if len(sys.argv) < 4:
    sys.stderr.write("Usage: bas2cas.py infile outfile filename6chars\n")
    return

  h = b'\x1F\xA6\xDE\xBA\xCC\x13\x7D\x74'
  if sys.argv[1] != '-':
    f = open(sys.argv[1], 'rb')
    try:
      data = f.read()
    finally:
      f.close()
  else:
    data = sys.stdin.buffer.read()

  if data[0:1] != b'\xFF':
    raise FileFormatError("The binary BASIC program does not start with 0FFh")

  data = data[1:] + b'\0\0\0\0\0\0\0'
  # Pad to a multiple of 8 bytes
  data = data + b'\0' * (-len(data) % 8)

  # Not the right encoding, so just in case, don't use special characters
  nam = ('%-6.6s' % sys.argv[3]).encode('latin1')

  if sys.argv[2] != '-':
    g = open(sys.argv[2], 'wb')
  else:
    g = sys.stdout.buffer
  try:
    # Block 1 (header)
    g.write(h)
    g.write(b'\xD3' * 10)
    g.write(nam)

    # Block 2 (data)
    g.write(h)
    g.write(data)
  finally:
    if sys.argv[2] != '-':
      g.close()

main()
