SetupIM2	proc

		; Abuse the 384-byte area at VOICAQ, VOICBQ, VOICCQ
		ld	hl,IntVec-1
		ld	(hl),195	; 11T ; jp NN
		ld	a,high _IM2Table
		ld	i,a
		ret

		rept	(high ($+255))*256-$
		nop
		endm

_IM2Table	rept	257
		db	high (IntVec-1)
		endm
		db	0,0		; Help debuggers synchronize

		endp
