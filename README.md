# VDP Timing Test

This program is designed to test the intricacies and fine details of how the VDP interacts with the CPU, especially in areas of possible interest to emulator authors, and also to MSX/MSX2/MSX2+/TR coders that want to make the most out of the VDP to e.g. squeeze as many transfers as possible in the shortest time.

It's currently work in progress.

An important detail is that it's designed to run on MSX machines with synced CPU/VDP clocks, not separate. This makes things hard for a particular VDP, the Toshiba T6950, at least in PAL machines which need a 22.168 MHz crystal that is not compatible with the regular CPU timings, and therefore always includes separate crystals for CPU and VDP. That leaves several popular Sony machines basically untestable with any degree of reliability.

It's also designed to run on machines with Z80 at 3.579545 MHz and with one M1 wait state, i.e. normal models with no turbo and no R800 mode. It uses ports 98h/99h, it does not read the port number from the BIOS, therefore it will only work in machines that use these ports.

Requires at least 32K to run, and a BASIC ROM (no CBIOS).

See the [Releases](https://notabug.org/pgimeno/vdptest/releases) page for pre-built binaries. Read USAGE.txt for more information.

The license for this code is the Expat license. See the file [LICENSE.md](https://notabug.org/pgimeno/vdptest/src/master/LICENSE.md) for details.
