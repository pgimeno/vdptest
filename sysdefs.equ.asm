; System variables used

KILBUF		equ	0156h		; Clear keyboard buffer

RG0SAV		equ	0F3DFh
RG08SAV		equ	0FFE7h
RG00SAV		equ	RG08SAV-8	; Fake system variable so we can add the register number to it
VALTYP		equ	0F663h
DAC		equ	0F7F6h
; We use this as our variables area. It's a 384 byte area that isn't used
; unless the BASIC PLAY command is used.
VOICAQ		equ	0F975h
HIMEM		equ	0FC4Ah
