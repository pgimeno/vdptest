includes := sysdefs.equ.asm\
 main.code.asm\
 main.bss.asm\
 im2setup.equ.asm\
 im2setup.code.asm\
 frametime.code.asm\
 frametime.bss.asm\
 syncvint.code.asm\
 test-ack-timing.code.asm\
 test-ack-timing.bss.asm\
 test-vram-timing.code.asm\


zipped := USAGE.txt LICENSE.md\
 vdptest.asm\
 sysdefs.equ.asm\
 main.code.asm\
 main.bss.asm\
 im2setup.equ.asm\
 im2setup.code.asm\
 frametime.code.asm\
 frametime.bss.asm\
 syncvint.code.asm\
 test-ack-timing.code.asm\
 test-ack-timing.bss.asm\
 test-vram-timing.code.asm\
 loadertpl.py asc2cld.py\
 blank1.dsk.gz blank2.dsk.gz\
 Makefile asc2cas.py raw2cas.py raw2bin.py\
 vdptest.cas vdptest.bas vdptest.bin vdptest1.dsk vdptest2.dsk\


orgdsk := 0x9000
orgcas := $(orgdsk)

all: vdptest.cas vdptest.bas vdptest.bin vdptest1.dsk vdptest2.dsk vdptest.zip

zip: vdptest.zip

cas: vdptest.cas

bas: vdptest.bas

bin: vdptest.bin

dsk: vdptest.bas vdptest.bin vdptest1.dsk vdptest2.dsk

vdptest.zip: $(zipped)
	rm -f $@
	zip $@ $+

vdptest1.dsk: vdptest.bas vdptest.bin
	gzip -cd blank1.dsk.gz > $@
	mcopy -i $@ vdptest.bas ::
	mcopy -i $@ vdptest.bin ::

vdptest2.dsk: vdptest.bas vdptest.bin
	gzip -cd blank2.dsk.gz > $@
	mcopy -i $@ vdptest.bas ::
	mcopy -i $@ vdptest.bin ::

loader.cld: loadertpl.py vdptestc.sym
	python3 $< cas $(orgcas) > $@ || rm -f $@

loader.cas: loader.cld
	python3 cld2cas.py $< $@ VDPTST

vdptest.cas: vdptest.raw loader.cas
	python3 raw2cas.py $< - VDPtst $(orgcas) | cat loader.cas - > $@

vdptest.bas: loadertpl.py vdptestd.sym
	python3 $< dsk $(orgdsk) > $@

vdptest.bin vdptestd.sym: vdptest.asm $(includes)
	pasmo --alocal --msx --equ origin=$(orgdsk) $< $@ vdptestd.sym

vdptest.raw vdptestc.sym: vdptest.asm $(includes)
	pasmo --alocal --bin --equ origin=$(orgcas) $< $@ vdptestc.sym

check:
	cd test_asc2cld && make $@ && cd ..

clean-base:
	rm -f vdptest.cas vdptest.bas vdptest.bin vdptest.raw \
		loader.cas loader.cld vdptest.bas vdptestc.sym vdptestd.sym \
		vdptest1.dsk vdptest2.dsk vdptest.zip

clean: clean-base
	cd test_asc2cld && make $@ && cd ..

distclean: clean-base
	cd test_asc2cld && make $@ && cd ..

.PHONY: all zip bas bin cas dsk check clean-base clean distclean
