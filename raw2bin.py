#!/usr/bin/env python3

# Written by Pedro Gimeno
# args: infile outfile startaddr [execaddr]
#       execaddr defaults to startaddr
# For lack of knowledge, currently only executable binaries are supported.

import sys, struct

def main():
  if len(sys.argv) < 4:
    sys.stderr.write("Usage: raw2bin.py infile outfile startaddr [execaddr]\n")
    return

  start = int(sys.argv[3], 0) & 0xFFFF
  if len(sys.argv) > 4:
    exe = int(sys.argv[4], 0)
  else:
    exe = start

  if sys.argv[1] != '-':
    f = open(sys.argv[1], 'rb')
    try:
      data = f.read()
    finally:
      f.close()
  else:
    data = sys.stdin.read()

  end = start + len(data) - 1

  if sys.argv[2] != '-':
    g = open(sys.argv[2], 'wb')
  else:
    g = sys.stdout.buffer
  try:
    g.write(b'\xFE')
    g.write(struct.pack('<HHH', start, end, exe))
    g.write(data)
  finally:
    if sys.argv[2] != '-':
      g.close()

main()
